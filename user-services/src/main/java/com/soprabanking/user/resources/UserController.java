package com.soprabanking.user.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.okta.sdk.client.Client;
import com.okta.sdk.resource.user.User;
import com.okta.sdk.resource.user.UserBuilder;
import com.okta.sdk.resource.user.UserList;
import com.soprabanking.user.model.UserDetails;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/pharmacy/user")
public class UserController {

	// Adding default slf4j logging
	Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	public Client client;
	
	@GetMapping("/basicAuth")
	public String login() {
		return "authenticated successfully";
	}

	@GetMapping("/")
	public String home(@AuthenticationPrincipal OidcUser user) {
		return "Welcome, " + user.getFullName() + "!";
	}

	@GetMapping("/getAllUsers")
	public UserList getUsers() {
		log.info("User list is asked to be retrieved");
		return client.listUsers();
	}

	@GetMapping("/user")
	public UserList searchUserByEmail(@RequestParam String query) {
		return client.listUsers(query, null, null, null, null);
	}

	@PostMapping("/addNewUser")
	public User saveNewUser(@RequestBody UserDetails userDetails) {
		User newUser = UserBuilder.instance().setEmail(userDetails.getEmail()).setFirstName(userDetails.getFname())
				.setLastName(userDetails.getLname()).setPassword(userDetails.getPassword().toCharArray())
				.setActive(true).buildAndCreate(client);
		log.info("User: " + newUser.getProfile() + " has been added");
		return newUser;
	}

}
